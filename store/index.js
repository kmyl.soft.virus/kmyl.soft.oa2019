import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
		//	用户信息
        userinfo: {
			id: '',
			uid: '',
			username: '',
			password: '',
			photo: '',
			nickname: '',
			role: [],
			loginTime: Date.now(),
			loginID: '',
			dep: { id: '', name: '' }
		}
    },
    mutations: {
        setUserInfo(state, userinfo) {
			if (!userinfo) return;
            state.userinfo = userinfo;
			try {
				uni.setStorageSync('userinfo', JSON.stringify(userinfo));
			} catch (e) {
				uni.showToast('持久化用户信息失败');
			}
        },
        logout(state) {
            state.userinfo = undefined;
			uni.removeStorageSync('userinfo');
        }
		
    },
	getters:{
		getUserInfo(state){
			try{
				let result = uni.getStorageSync('userinfo');
				if (result){
					result = JSON.parse(result);
				}
				return result;
			}catch(e){
				return undefined;
			}
		}
	}
});

export default store;
