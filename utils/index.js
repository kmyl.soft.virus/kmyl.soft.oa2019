import md5 from './md5.js';

const BASEURL = 'http://www.cnynld.com:8081/a';
const OAURL = 'http://www.cnynld.com:9000';
const markey = 'ERMwlqouGthgpNikLVC4IzO9sWg2SmTHFUbx7eaXdZDv568AfcQnJr10KY3yPB';


async function request(method = 'GET', url, data, header){
	if (data){
		let str = '';
		for (let key in data){
			str += `${ key }=${ data[key] }&`;
		}
		str += 'merkey=' + markey;
		data.mac = md5(str);
	}
	return new Promise((resolve, reject) => {
		uni.request({ method, url, data, header: Object.assign({'content-type': 'application/x-www-form-urlencoded' }, header || {}),
			success: (res) => {
				resolve(res.data);
			},
			fail:(res) => {
				reject();
			}
		});
		
	});
}

async function GET(url, data){
	return await request('GET', url, data);
}

async function POST(url, data){
	return await request('POST', url, data);
}


export default {
	BASEURL,
	OAURL,
    GET,
    POST
}
