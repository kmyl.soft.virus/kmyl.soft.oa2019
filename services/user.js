/**
 * 功能：用户请求相关
 * action: login -- 登录 -- 胡文斌 -- 2018.11.25 17:10
 * 
 */
import utils from '../utils';

/**
 * 登录
 * @description 登录loopback和jeestie
 * @param username {String} 用户名
 * @param password {String} 密码
 * @returns {Promise<Object>} 异步方法，返回用户信息
 * @author 胡文斌
 */
async function login(username, password){
	try{
		let result = await utils.POST(utils.BASEURL + '/mobileApp/checkuser', { username, password });
		if (!result.success){
			uni.showToast({ icon: 'none', title: result.msg || '' });
			return undefined;
		}
		let state = {
			id: result.obj.no,
			uid: result.obj.id,
			username,
			password,
			photo: result.obj.photo,
			nickname: result.obj.name,
			role: result.obj.roleNames.split(','),
			loginTime: Date.now()
		};
		
		result = await utils.POST(utils.OAURL + '/outBaseController.app', { userloginbyno: '', userno: state.id });
		if (!result.success){
			uni.showToast({ icon: 'none', title: result.msg || '' });
			return undefined;
		}
		if(!result.obj.id) {
			uni.showToast("业务ID暂未设置，请联系管理员! ");
			return undefined;
		}
		state.loginID = result.obj.id;
		state.dep = {
			id: result.obj.tsdepart.id,
			name: result.obj.tsdepart.departname
		};
		return state;
	}catch(e){
		uni.showToast(e.message);
		console.log(e);
		return undefined;
	}
}


async function getAlluser(id){
	try{
		let resutl = await utils.POST(utils.BASEURL + '/mobileApp/GetAlluserNoMy', { myNo: id });
		if (resutl.success){
			let list = [];
			for (let item of resutl.obj){
				list.push({
					id: item.mongoinfobase.staffNo,
					uid: item.mysqlid,
					photo: item.userimg,
					name: item.mongoinfobase.staffName,
					sex: item.mongoinfobase.sex === '男',
					query: item.pinyin + item.pinyinAll + item.mongoinfobase.staffName,
					dep: item.mongouserPosition.department,
					phone: item.mongoinfoconnect.cel,
					email: item.mongoinfoconnect.email,
					qq: item.mongoinfoconnect.qq,
					wechat: item.mongoinfoconnect.wechat
				});
			}
			return list;
		}else{
			uni.showToast(resutl.msg);
			return [];
		}
	}catch(e){
		uni.showToast(e.message);
		return [];
	}
}


export default {
	login,
	getAlluser
}